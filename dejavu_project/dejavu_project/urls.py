"""dejavu_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from rest_framework_swagger.views import get_swagger_view
from rest_framework.routers import DefaultRouter

schema_view = get_swagger_view(title='Pastebin API')

from dejavu.views import *

router = DefaultRouter()
router.register(prefix='venues', viewset=venueviewsets)
router.register(prefix = 'events',viewset=eventviewsets)
router.register(prefix = 'music_types',viewset=musictypeviewsets,base_name='test')
router.register(prefix = 'event_photos',viewset = eventimageviewsets)
router.register(prefix = 'venue_photos',viewset = venueimageviewsets)
router.register(prefix = 'app_users',viewset = appuserviewset, base_name='app_users')
router.register(prefix = 'countries',viewset = countryviewset)
router.register(prefix = 'contact_dejavu',viewset = contactowner)




urlpatterns = router.urls

urlpatterns = urlpatterns+ [
    url(r'^swag$', schema_view)
]

urlpatterns = urlpatterns + [
    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns  = urlpatterns+ [
    url(r'^admin/', include(admin.site.urls)),
]


urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls',namespace='rest_framework')),
]

