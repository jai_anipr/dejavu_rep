from .models import *

from rest_framework import serializers
from django.contrib.auth.models import User,Group

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class appuserSerializer(serializers.ModelSerializer):
    class Meta:
        model = application_user
        fields = '__all__'


class venueSerializer(serializers.ModelSerializer):
    class Meta:
        model = venue
        fields = '__all__'


class eventSerializer(serializers.ModelSerializer):
    class Meta:
        model = event
        fields = '__all__'

class venuephotosSerializer(serializers.ModelSerializer):
    class Meta:
        model = venuephotos
        fields = '__all__'

class eventphotosSerializer(serializers.ModelSerializer):
    class Meta:
        model = eventphotos
        fields = '__all__'

class musicSerializer(serializers.ModelSerializer):
    class Meta:
        model = music_type
        fields = '__all__'


class countryserializer(serializers.ModelSerializer):
    class Meta:
        model = Country

class appownerSerializer(serializers.ModelSerializer):
    class Meta:
        model = appowner
        fields = '__all__'
