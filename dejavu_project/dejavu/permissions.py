from rest_framework import permissions
from rest_framework.permissions import BasePermission, SAFE_METHODS
from django.contrib.auth.models import Group


# customers_group = Group.objects.get(name = 'customers')
class Isuser(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user and request.user.groups.filter(name='users'):
            return True
        return False


class Isappusers(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user:
            if request.user.groups.filter(name='app users'):
                return True
            return False
