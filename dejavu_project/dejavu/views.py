from django.shortcuts import render

# Create your views here.

from dejavu.models import application_user,venue,event,eventphotos,venuephotos,music_type,Country,appowner

from .serializers import appuserSerializer,UserSerializer,venueSerializer,eventSerializer,eventphotosSerializer,venuephotosSerializer,musicSerializer,countryserializer,appownerSerializer

from rest_framework import viewsets
from django.contrib.auth.models import User,Group

from rest_framework.authtoken.models import Token

import jwt
import datetime
from django.conf import settings
from django.template import Context
from dejavu_project.settings import SITE_URL
from smtplib import SMTP
from rest_framework import response,status

class appuserviewset(viewsets.ModelViewSet):
    queryset = application_user.objects.all()
    serializer_class = appuserSerializer
    #permission_classes = '----------------------'

    def list(self, request, *args, **kwargs):
        try:
            queryset = application_user.objects.all()
            serializer = appuserSerializer(queryset,many=True)
            for item in serializer.data:
                user = User.objects.filter(id=item['app_user'])
                details = UserSerializer(user,many=True)
                item['userdetails'] = details.data
            return response.Response(serializer.data,status=status.HTTP_200_OK)
        except Exception as e:
            return  response.Response({'error message':e}, status = status.HTTP_400_BAD_REQUEST)


    def create(self, request, *args, **kwargs):
        try:
            user = User.objects.create_user(request.data['username'],request.data['email'],request.data['password'])
            if 'first_name' in request.data:
                user.first_name = request.data['first_name']
            if 'last_name' in request.data:
                user.last_name = request.data
            user.is_active = False

            try:
                user.save()
                user_details = application_user(app_user= user)
                if 'app_user_contact' in request.data:
                    user_details.app_user_contact = request.data['app_user_contact']
                if 'app_gender' in request.data:
                    user_details.app_gender = request.data['app_gender']
                if 'country' in request.data:
                    user_details.country = Country.objects.get(id = int(request.data['country']))

                token = jwt.encode({
                    'name' : 'user.username',
                    'iat' : datetime.datetime.utcnow(),
                    'nbf' : datetime.datetime.utcnow() + datetime.timedelta(minutes=5),
                    'exp': datetime.datetime.utcnow() + datetime.timedelta(days=5)},settings.SECRETKEY)
                user_details.save()
                try:
                        content = Context({
                            'first_name' : user.first_name,
                            'token': token,
                            'url': SITE_URL


                        })

                        def sendemail(from_addr, to_addr_list, cc_addr_list,subject, message,login, password,smtpserver='smtp.gmail.com:587'):


                            message = message
                            subject = subject
                            message1 = 'Subject: {}\n\n{}'.format(subject, message)
                            server = SMTP(smtpserver)
                            server.starttls()
                            server.login(login, password)
                            problems = server.sendmail(from_addr, to_addr_list, message1)
                            server.quit()

                        sendemail(from_addr='jai@anipr.in',
                                  to_addr_list=[user.email],
                                  cc_addr_list=[''],
                                  subject='Confirm mail',
                                  message= content,
                                  login='jaisimha15@gmail.com',
                                  password='')
                        # for zoho server = smtplib.SMTP_SSL('smtp.zoho.com', 465)

                        return response.Response({'Success': 'Email successfully sent'}, status=status.HTTP_200_OK)
                except Exception as e:
                    print e
                    return response.Response({'Error': e.message}, status=status.HTTP_400_BAD_REQUEST)
            except Exception as v:
                print v
                return response.Response({'status':'email already exists'},status = status.HTTP_400_BAD_REQUEST )
        except Exception as k:
            print k
            return  response.Response({'status':'account already exists'}, status = status.HTTP_400_BAD_REQUEST)

    def confirm_email(self, request):
        try:
            if 'token' in request.data:
                token = request.data['token']
                payload = jwt.decode(token, settings.SECRET_KEY)
                user = User.objects.get(username=payload['username'])
                user.is_active = True
                user.save()
            return response.Response({'Success': 'Your account has been activated'}, status=status.HTTP_202_ACCEPTED)
        except Exception, e:
            print (e)
            return response.Response({'Error': 'Invalid token'}, status=status.HTTP_400_BAD_REQUEST)



#retrieve user included in modelviewsets and need not modify
#delete- indcluded in modelviewsets its enough to add permissions

class musictypeviewsets(viewsets.ModelViewSet):
    queryset = music_type.objects.all()
    serializer_class = musicSerializer
    def all_music(self, request):
        try:
            musicqueryset = music_type.objects.all()
            musicserializer = musicSerializer(musicqueryset,many=True)
            return response.Response(musicserializer.data,status = status.HTTP_200_OK)
        except Exception as e:
            return response.Response({'error message':e},status=status.HTTP_400_BAD_REQUEST)



class venueviewsets(viewsets.ModelViewSet):
    queryset = venue.objects.all()
    serializer_class = venueSerializer

    def list(self, request, *args, **kwargs):

        try:
            queryset = venue.objects.all()
            serializer = venueSerializer(queryset,many=True)

            for item in serializer.data:
                venue_images_object = venuephotos.objects.filter(id = item['venue_photos'])
                serializer1 = venuephotosSerializer(instance=venue_images_object,many=True)
                item['venue_image_details'] = serializer1.data
            return response.Response(serializer.data,status=status.HTTP_200_OK)
        except Exception as e:
            return response.Response({'error message':e},status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, *args, **kwargs):

        try:
            venue_object = self.get_object()
            queryset = venue.objects.filter(pk = venue_object.id)

            serializer = venueSerializer(instance=queryset,many=True)
            for item in serializer.data:
                venueimageobject = venuephotos.objects.filter(id= item['venue_photos'])
                serializer2 = venuephotosSerializer(instance=venueimageobject,many=True)
                item['venue_image_details'] = serializer2.data
                venueeventobject = event.objects.filter(id = venue_object.id)
                serializer3 = eventSerializer(instance=venueeventobject,many=True)
                item['events_at_venue'] = serializer3.data
                venueeventphotoobject = eventphotos.objects.filter(id = venueeventobject[0].id)
                serializer4 = eventphotosSerializer(instance=venueeventphotoobject,many=True)
                item['venue-event photos'] = serializer4.data



            return response.Response(serializer.data,status=status.HTTP_200_OK)
        except Exception as e:
            return response.Response({'error message': e},status=status.HTTP_400_BAD_REQUEST)




"""
    def list(self, request, *args, **kwargs):
        try:
            queryset =venue.objects.all()
            serializer = venueSerializer(queryset,many=True)

            for temp in serializer.data:

                venue_pics =[]


                try:
                    for photo in temp['venue_photos']:
                        images ={}
                        venue_object = venuephotos.objects.get(id= photo)
                        images['id'] = venue_object.id
                        images['image'] = venue_object.venue_image
                        venue_pics.append(images)
                    temp['venue_image_list'] = venue_pics
                except Exception as e:
                    return response.Response({'error message':e}, status= status.HTTP_400_BAD_REQUEST)
            return response.Response(serializer.data,status= status.HTTP_200_OK)
        except Exception as e:
        
            return response.Response({'error message': e},status=status.HTTP_400_BAD_REQUEST)
"""
"""
    def retrieve(self, request, *args, **kwargs):
        venue_object = self.get_object()
        queryset = venue.objects.filter(pk = venue_object.id )

        serializer = venueSerializer(instance=queryset,many=True)
        for temp in serializer.data:
            
            
"""




"""
    def create(self, request, *args, **kwargs):
        try:
            class countryviewset(viewsets.ModelViewSet):
                queryset = Country.objects.all()
                serializer_class = countryserializer
"""

from rest_framework.decorators import list_route,detail_route
class eventviewsets(viewsets.ModelViewSet):
    queryset = event.objects.all()
    serializer_class = eventSerializer

    def list(self, request, *args, **kwargs):
        try:
            queryset = event.objects.all()
            serializer = eventSerializer(queryset,many=True)

            for temp in serializer.data:
                venue_object = venue.objects.filter(id=temp['event_venue'])
                music_object = music_type.objects.filter(id = temp['event_musictype'])
                serializer1 =  venueSerializer(venue_object,many=True)
                serializer2 = musicSerializer(music_object, many=True)
                temp['venue_details'] = serializer1.data
                temp['event_music_details'] = serializer2.data
            return response.Response(serializer.data,status=status.HTTP_200_OK)
        except Exception as e:
            return  response.Response({'error message':e},status= status.HTTP_400_BAD_REQUEST)


    def retrieve(self, request, *args, **kwags):

        event_object = self.get_object()
        queryset = event.objects.filter(pk= event_object.id)
        #serializer_class = eventSerializer

        serializer = eventSerializer(instance=queryset,many=True)
        for temp in serializer.data:
            venue_object = venue.objects.filter(id=temp['event_venue'])
            music_object = music_type.objects.filter(id=temp['event_musictype'])
            serializer1 = venueSerializer(venue_object, many=True)
            serializer2 = musicSerializer(music_object, many=True)
            temp['venue_details'] = serializer1.data
            temp['event_music_details'] = serializer2.data
        return response.Response(serializer.data, status=status.HTTP_200_OK)




        #if 'event_venue' in  serializer.data:
         #   venue_object = venue.objects.filter(id = id['event_venue'] )
          #  serializer1 = venueSerializer(venue_object, many=True)
           # serializer.data['venue_details'] = serializer1.data
        #return response.Response(serializer.data,status= status.HTTP_200_OK)



#   def retrieve_object(self, request,pk):
#        queryset = event.objects.filter(id = pk)
#        serializer = eventSerializer(queryset,many=True)


"""
        if 'event_venue' in serializer.data:
            venue_object = venue.objects.filter(id=int(event['event_venue']))
            music_object = music_type.objects.filter(id=music_type['event_musictype'])
            serializer1 = venueSerializer(venue_object, many=True)
            serializer2 = musicSerializer(music_object, many=True)
            serializer['venue_details'] = serializer1.data
            serializer['event_music_details'] = serializer2.data
        return response.Response(serializer.data, status=status.HTTP_200_OK)

"""
"""
        print ("venue id", int(request.data['venue_id']))
        venue_id = int(request.data['feedback_id'])

        try:
            queryset = event.objects.filter(id=venue_id)
            serializer = eventSerializer(queryset, many=True)

"""



class countryviewset(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = countryserializer

class contactowner(viewsets.ModelViewSet):
    queryset = appowner.objects.all()
    serializer_class = appownerSerializer

    def create(self, request, *args, **kwargs):

        data= request.data
        contact_object = appowner()
        try:
            if 'name' in data:
                contact_object.name = data['name']
            if 'email' in data:
                contact_object.email = data['email']
            if 'message' in data:
                contact_object.message = data['message']
            contact_object.save()
            try:


                def sendemail(from_addr, to_addr_list, cc_addr_list, subject, message, login, password,
                              smtpserver='smtp.gmail.com:587'):

                    message = message
                    subject = subject
                    message1 = 'Subject: {}\n\n{}'.format(subject, message)
                    server = SMTP(smtpserver)
                    server.starttls()
                    server.login(login, password)
                    problems = server.sendmail(from_addr, to_addr_list, message1)
                    server.quit()

                sendemail(from_addr='jaisimha15@gmail.com',
                          to_addr_list='jai@anipr.in',
                          cc_addr_list=[''],
                          subject='Contact message mail',
                          message= 'Name : '+contact_object.name +'\n\n'+ 'Email : ' +contact_object.email+ '\n\n'+ 'Message : '+contact_object.message ,
                          login='jaisimha15@gmail.com',
                          password='')
                # for zoho server = smtplib.SMTP_SSL('smtp.zoho.com', 465)

                return response.Response({'Success': 'Email successfully sent'}, status=status.HTTP_200_OK)
            except Exception as e:
                print e
                return response.Response({'Error': e.message}, status=status.HTTP_400_BAD_REQUEST)



        except Exception as e:
            return response.Response({'erro':e.message},status=status.HTTP_400_BAD_REQUEST)



class eventimageviewsets(viewsets.ModelViewSet):
    queryset = eventphotos.objects.all()
    serializer_class = eventphotosSerializer

class venueimageviewsets(viewsets.ModelViewSet):
    queryset = venuephotos.objects.all()
    serializer_class = venuephotosSerializer






























