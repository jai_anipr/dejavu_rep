from django.contrib import admin

# Register your models here.


from .models import *

admin.site.register(venue)
admin.site.register(event)
admin.site.register(venuephotos)
admin.site.register(eventphotos)
admin.site.register(application_user)
admin.site.register(music_type)
