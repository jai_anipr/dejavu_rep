from django.db import models

# Create your models here.

from django.contrib.auth.models import User,Group

class eventphotos(models.Model):
    event_image = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)


from django.utils.encoding import python_2_unicode_compatible
#@python_2_unicode_compatible
class venuephotos(models.Model):
    #venue_image = models.ImageField(upload_to= None,max_length=200)
    venue_image = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

 #   def __str__(self):
  #      return self.venue_image



class music_type(models.Model):
    music = models.CharField(max_length=200)
    music_description = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

class venue(models.Model):
    venue_name = models.CharField(max_length=200)
    venue_adress = models.CharField(max_length=200)
    venue_phone = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now=True)
    description = models.TextField()
    venue_photos = models.ForeignKey(venuephotos)
    updated_at = models.DateTimeField(auto_now_add=True)

class event(models.Model):
    event_name = models.CharField(max_length=200)
    event_venue = models.ForeignKey(venue)
    event_phone = models.CharField(max_length=200)
    event_email = models.CharField(max_length=200)
    event_date = models.DateField()
    event_time  = models.DateTimeField()
    event_description = models.TextField()
    event_musictype = models.ForeignKey(music_type)
    event_images = models.ForeignKey(eventphotos)
    event_iscompleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

class Country(models.Model):
    country_name = models.CharField(max_length=50, null=True, blank=True)

class appowner(models.Model):
    name = models.CharField(max_length=128, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    message = models.TextField(null=True, blank=True)


class application_user(models.Model):
    app_user = models.ForeignKey(User)
    app_user_contact = models.CharField(max_length=100,default='')
    app_gender = models.CharField(max_length=100,blank=True,null=True )
    country = models.ForeignKey(Country,null=True,blank=True)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
















